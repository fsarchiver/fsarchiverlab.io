serve:
	hugo server --bind=0.0.0.0

clean:
	rm -rf public

build: clean
	hugo --logLevel info
